const axios = require('axios');
const crypto = require('crypto');

//const parseString = require('xml2js').parseString;

const Simbla = require("parse/node");

Simbla.serverURL = process.env.serverURL || "https://apps.simbla.com/parse";

const SaleRows = Simbla.Object.extend("SaleRows");
const Sales = Simbla.Object.extend("Sales");
//const SaleStatuses = Simbla.Object.extend("SaleStatuses");
//const Products = Simbla.Object.extend("Products");
const Account = Simbla.Object.extend("Accounts");

const VoucherDiscountProductId = "8YmC6rD7wN";

const ReturningContactDiscountProductId = "hNl5FXy8OR";


const ENV_KEY = "";//"sandbox-"; // "";
const API_URI = "https://" + ENV_KEY + "api.paysimple.com/v4/";// "https://api.paysimple.com/v4/"
const CONFIG_KEY = "paysimple_" + ENV_KEY + "cred";// "paysimple_cred";
const ACC_KEY = "paysimple_" + ENV_KEY.replace("-", "_") + "customerId";// "paysimple_customerId"
const ACC_CC_KEY = "paysimple_" + ENV_KEY.replace("-", "_") + "accountId";// "paysimple_accountId"

axios.defaults.baseURL = API_URI;

//const USDCODE = "01";
//const EURCODE = "27";
//const ILSCUDE = "X";

const CURRENCIES = Simbla.Object.extend("Currencies");
const CURRENCY_USD = "qYWiWHBBQ4";
const CURRENCY_IL = "gH28WsYLWS";
//const CURRENCY_EUR = "4I2J3hOTLU";

const ReceiptId = "oGSYyZhYmi",
    //TaxInvoiceId = "UMoClSkKPk",
    //ProformaInvoiceId = "Gq6CPUYQFw",
    //RefundInvoiceId = "8bThnGEPsc",
    TaxInvoiceReceiptId = "rOTjYAZGYj",
    InvoiceReceiptTemplateId = "012UCjoGNI",
    InvoiceReceiptTemplateIdEN = "WBBTBFm9bz",

    //ReceiptTemplateId = "T6kEOOWPPR",
    InvoiceTemplateId = "oaD0Ns4iq6";



function checkPeleCardType(pelecardResNum) {
    switch (pelecardResNum) {
        case "1":
            return "EeaidU2JGo" /// ישראכארד
        case "2":
            return "IZHZesZD4R" // ויזה
        case "3":
            return "215eyfK3EQ" /// דיינקס
        case "4":
            return "toRlw17TqH" // אמריקן
        case "6":
            return "Hb7ogD9PzZ" /// לאומי
    }
}

function dataForCreateHebDocument(params, invoice, lines) {
    let isInvoiceOnly = false; //invoice && invoice.get("DocTypeId") && invoice.get("DocTypeId").id === TaxInvoiceId
    // create recipt/invoicerecipt
    //
    let data = {
        templateId: isInvoiceOnly ? InvoiceTemplateId : (params.EN_temp ? InvoiceReceiptTemplateIdEN : InvoiceReceiptTemplateId),
        DocTypeId: isInvoiceOnly ? ReceiptId : TaxInvoiceReceiptId,
        Name: params.name || "",
        CreateNewAccount: 0,
        AccountId: !invoice ? params.accountId : invoice.get("AccountId").id,
        Address: params.address || "",
        CompanyId: "",
        Zip: params.zip || "",
        Country: params.country || "",
        State: params.state || "",
        City: params.city || "",
        PhoneNumber: params.phone || "",
        DocumentDetails: params.DocumentDetails || "",
        TotalBeforeDiscount: !invoice ? params.TotalBeforeDiscount : !isInvoiceOnly ? invoice.get("TotalBeforeDiscount") : undefined,
        DiscountValue: !invoice ? params.DiscountValue : !isInvoiceOnly ? invoice.get("DiscountValue") : undefined,
        TotalAfterDiscount: !invoice ? params.TotalAfterDiscount : !isInvoiceOnly ? invoice.get("TotalAfterDiscount") : undefined,
        VatPercent: !invoice ? params.VatPercent : !isInvoiceOnly ? invoice.get("VatPercent") : undefined,
        VatValue: !invoice ? params.VatValue : !isInvoiceOnly ? invoice.get("VatValue") : undefined,
        TotalSum: params.totalSum,
        Email: params.email || "",
        NoVat: !invoice ? params.NoVat : !isInvoiceOnly ? invoice.get("NoVat") : undefined,
    }

    let InvoiceLines = [];
    if (!isInvoiceOnly) {

        lines.filter(line => (line.get("RowTotal") || line.get("Total")) !== 0).sort((a, b) => {
            return (a.get("InvoiceOrder") || 0) - (b.get("InvoiceOrder") || 0)
        }).forEach(row => {

            InvoiceLines.push({
                ProductId: row.get("ProductId") ? row.get("ProductId").id : row.get("Product") ? row.get("Product").id : undefined,
                ProductDescription: row.get("Description") || "",
                Quantity: row.get("Quantity"),
                CurrencyRate: row.get("CurrencyRate") || 1,
                CurrencyId: (row.get("CurrencyId") && row.get("CurrencyId").id) || CURRENCY_IL,
                Price: (row.get("Price") || ((row.get("Total") || row.get("RowTotal")) / row.get("Quantity")) || 0) * (params.priceFactor || 1),
                RowTotal: (row.get("RowTotal") || row.get("Total")) * (params.priceFactor || 1)
            });
        });
    }

    data.InvoiceLines = InvoiceLines;


    data.ReceiptLines = [{
        PaymentSum: params.totalSum,
        PaymentTotalSum: params.totalSum,
        PaymentCurrencyRate: 1,
        PaymentCurrency: CURRENCY_IL, //  get INS id
        PaymentRowType: "Rl0OajXxax", //cc id
        PaymentDescription: "אשראי | " + params.last4digit,
        CreditCardType: cardType,
        CreditCardPaymentType: 'DebitRegularType',
        PaymentDate: new Date(),
        
        DebitApproveNumber: params.DebitApproveNumber

    }];
    return data;
}

exports.function = async (req, res) => {

    async function callAPI(endPoint, body, method = "get") {
        const response = await axios[method](endPoint, body);

        //console.log(response.data);
        if (response.status > 299 || (response.data.Meta && response.data.Meta.HttpStatus > 299)) {
            throw response.data.Meta.Errors;
        }
        return response.data.Response || response.data;

    }
    // async function getCurrentcyRate(curr = EURCODE) {
    //     if (curr === ILSCUDE)
    //         return 1;
    //     let currXml = await callAPI("https://www.boi.org.il/currency.xml?curr=" + curr);
    //     return new Promise((resolve, reject) => {
    //         parseString(currXml, { explicitArray: false }, function (err, data) {
    //             if (err)
    //                 return reject(err);
    //             resolve(Number(data.CURRENCIES.CURRENCY.RATE));
    //         })
    //     });
    // }
    async function getCardType(cc = "4580458045804580", exp = '1223', cvv = '123', y = '123456789', token = "") {
        const conf = await (new Simbla.Query("Config")).equalTo("Name", "Payment").equalTo("Value.method", "pelecard").equalTo("Value.active", true).first({ useMasterKey: true });
        let value = conf.get("Value") || {};
        const data = await callAPI("https://gateway20.pelecard.biz/services/DebitByIntIn", {
            "terminalNumber": (value.secret && value.secret.terminalNumber) || "0962210",
            "user": (value.secret && value.secret.user) || "PeleTest2",
            "password": (value.secret && value.secret.password) || "Pelecard1Test",
            // PARAMS description
            // B card number
            // T expires
            // D as is - transaction type
            // U cvv
            // Y ID
            // I קופה 001
            // C - sum in agorot
            // J - 2!!!! type check card
            // X - test of any additional number
            "intIn": `B${cc}T${exp}D011150U${cvv}Y${y}I001C100J2Xtest`,
            "token": token

        }, "post");

        console.log(data)
        if (!["000", "041", "042"].includes(data.StatusCode) || !data.ResultData || !data.ResultData.CreditCardAbroadCard) {
            throw data.ErrorMessage
        }
        var type = data.ResultData.CreditCardAbroadCard;
        console.log('type', type)

        const isDiaspora = type == "1";
        // 0 israe
        // 1 for tourist
        return isDiaspora;


    }

    try {


        // init simbla with current application data
        Simbla.initialize(req.body.applicationId, undefined, req.body.masterKey);
        delete req.body.applicationId;
        delete req.body._ApplicationId;
        delete req.body.masterKey;
        console.log(Simbla.applicationId);

        if (!req.body.user || !req.body.user.sessionToken)
            throw "missing sessionToken"


        let savedToken;
        if (req.body.ccToken) {
            // validate token
            savedToken = await Simbla.Object.extend("AccountTokens").createWithoutData(req.body.ccToken).fetch({ useMasterKey: true });
            if (!savedToken)
                throw "ccToken not found";


        }


        if ((!req.body.CreditCardNumber || req.body.CreditCardNumber.length < 14 || req.body.CreditCardNumber.length > 19) && !req.body.ccToken)
            throw "missing or invalid CreditCardNumber";



        if (!req.body.ExpirationDate && !req.body.ccToken)
            throw "missing ExpirationDate";
        if (!req.body.ccToken && (req.body.ExpirationDate.indexOf('/') !== 2 || req.body.ExpirationDate.length !== 7))
            throw "invalid ExpirationDate. format is mm/yyyy";



        if (req.body.checkCardType) {
            console.log('checkCardType')
            let isDiasporaCard = await getCardType(req.body.CreditCardNumber, req.body.ExpirationDate.substr(0, 2) + (req.body.ExpirationDate.substr(-2, 2)), req.body.CVV, undefined, savedToken && savedToken.get("Token"));
            return res.status(200).send({ isDiasporaCard });

        }
        if (!req.body.CVV && !req.body.ccToken)
            throw "invalid CVV";

        if ((!req.body.Issuer || !["Visa", "Master", "Amex", "Discover"].includes(req.body.Issuer)) && !req.body.ccToken)
            throw "invalid Issuer. valid values are Visa, Master, Amex and Discover";

        if (!req.body.ApprovedTerms) {
            throw "must approve terms";
        }

        let payments = parseInt(req.body.payments || 1, 10);




        // validate sale rows discounts
        const attachBody = {
            functionName: "kimama-attach-prod",
        }
        if (req.body.accountId)
            attachBody.accountId = req.body.accountId;
        if (req.body.saleId) {
            let q = new Simbla.Query('_Role').equalTo('users', Simbla.User.createWithoutData(req.body.user.objectId));
            q.select(["name"]);
            const _myRoles = await q.find({ sessionToken: req.body.user.sessionToken });
            if (!_myRoles.some(r => {
                let name = r.get("name");
                return name === "Sales" || name.toLowerCase().indexOf("admin") !== -1 || name === "CRM" || name === "Marketing" || name === "Support";
            }))
                throw "invalid role for user to use saleId";

            attachBody.saleId = req.body.saleId;
        }

        await Simbla.Cloud.run("run_function", attachBody, { sessionToken: req.body.user.sessionToken });

        let saleQ = (new Simbla.Query(Sales));
        if (req.body.saleId) {
            saleQ.equalTo("objectId", req.body.saleId)
        }
        else
            saleQ.equalTo("SaleStatusId", Simbla.Object.extend("SaleStatuses").createWithoutData("rGQ3Go7xVw"))
        let accountQ = (new Simbla.Query(Account)).include("CountryId");
        if (req.body.accountId) {
            saleQ.equalTo("AccountId", Simbla.Object.extend("Accounts").createWithoutData(req.body.accountId));
            accountQ.equalTo("objectId", req.body.accountId);
        }
        let [saleRows, account, sale] = await Promise.all([
            (new Simbla.Query(SaleRows)).matchesQuery("SaleId", saleQ).include("ProductId").include("ContactId").find({ sessionToken: req.body.user.sessionToken }),

            accountQ.first({ sessionToken: req.body.user.sessionToken }),

            saleQ.first({ sessionToken: req.body.user.sessionToken })
        ]);
        if (!account) {
            throw "account not found";
        }
        if (savedToken && savedToken.get("AccountId") && savedToken.get("AccountId").id !== account.id)
            throw "invalid token fot this account";

        if (saleRows.length === 0) {
            throw "no sales rows found"
        }
        if (!sale) {
            throw "sale not found";
        }
        console.log('sale', sale.id);
        let couponRow = saleRows.find(s => s.get("ProductId") && s.get("ProductId").id === VoucherDiscountProductId);
        let coupon;
        if (couponRow) {
            coupon = await couponRow.get("Coupon").fetch({ useMasterKey: true });
            if (!coupon || coupon.get("UsageAvailable") <= 0) {
                throw "invalid coupon";
            }
        }
        let isUSPurchase = saleRows.some(s => s.get("ProductId").get("Category") && s.get("ProductId").get("Category").id === "LnO0NmRVST");

        if (isUSPurchase && saleRows.some(s => s.get("ProductId").get("Category") && ["cumxY7RLoA", "jNBZmuddW8"].includes(s.get("ProductId").get("Category").id)))
            throw "Invalid mix of US and IL/EUR camps";

        const amount = saleRows.map(s => s.get("Total") || 0).reduce((p, c) => p + c, 0);

        if (amount <= 0)
            throw "invalid amount";

        let saleCurrncy;
        if (isUSPurchase) {
            if (req.body.ccToken) {
                throw "invlid use of token";
            }


            let conf = (await (new Simbla.Query("Config")).equalTo("Name", CONFIG_KEY).first({ useMasterKey: true }));

            if (!conf) {
                const err = "no conf found " + CONFIG_KEY;
                console.error(err);
                return res.status(400).send(err);
            }
            let confVal = conf.get("Value");
            const apiKey = confVal.apiKey;
            const userId = confVal.userId;
            if (!apiKey) {
                const err = "no apiKey"
                console.error(err);
                return res.status(400).send(err);

            }
            if (!userId) {
                const err = "no userId"
                console.error(err);
                return res.status(400).send(err);
            }

            const time = (new Date()).toISOString();

            const signature = crypto.createHmac('sha256', apiKey).update(time).digest("base64");

            const authHeader = `PSSERVER accessid=${userId}; timestamp=${time}; signature=${signature}`
            axios.defaults.headers.common['Authorization'] = authHeader;



            let paysimpleCustomerId;
            if (account.get(ACC_KEY)) {
                paysimpleCustomerId = account.get(ACC_KEY);
            }
            else {
                let customerData = {
                    FirstName: account.get("F_name") || account.get("Name").split(" ").pop(),
                    LastName: account.get("L_name") || account.get("familyName") || account.get("Name").split(" ").shift(),
                    ShippingSameAsBilling: true,
                    CustomerAccount: account.id,
                    Email: account.get("Email"),
                    AltEmail: account.get("emailParent2")
                }
                let customer = await callAPI("customer", customerData, "post");

                paysimpleCustomerId = customer.Id;
                await account.save({ [ACC_KEY]: paysimpleCustomerId }, { useMasterKey: true });

            }

            let newCCData = {
                BillingZipCode: req.body.BillingZipCode,
                CustomerId: paysimpleCustomerId,
                CreditCardNumber: req.body.CreditCardNumber,// || "4111111111111111",
                ExpirationDate: req.body.ExpirationDate,// || "12/2033",
                Issuer: req.body.Issuer,// || "Visa"
            }

            const psAccount = await callAPI("account/creditcard", newCCData, "post");

            const ccAccount = psAccount.Id;
            // save the account id?
            if (account.get(ACC_CC_KEY) !== ccAccount)
                await account.save({ [ACC_CC_KEY]: ccAccount }, { useMasterKey: true });


            saleCurrncy = CURRENCIES.createWithoutData(CURRENCY_USD)

            if (payments && payments > 1) {

                if (payments > 6)
                    throw "maximum payments are 6";

                // 
                let d = new Date();
                // first payment is today
                //let FirstPaymentDate = d.getFullYear() + '-' + (d.getMonth() + 1 + "").padStart('2', '0') + '-' + (d.getDate() + "").padStart('2', '0');

                let ExecutionFrequencyParameter = d.getDate();

                let firstPayment, otherPayments, StartDate, EndDate, paymentLeft;
                // after august and before march
                if (d.getMonth() > 7 || d.getMonth() < 2) {
                    let numOfContacts = (saleRows.filter(sr => !!sr.get("ContactId")).reduce((prev, cur) => {
                        prev.add(cur.get("ContactId").id);
                        return prev;
                    }, new Set())).size;
                    // first payment 300 for each children
                    firstPayment = 300 * numOfContacts;
                    if (amount < firstPayment)
                        throw "minimum first payment amount is " + firstPayment;


                    otherPayments = (amount - firstPayment) / (payments);

                    // set the first! payment to march
                    while (d.getMonth() < 2 || d.getMonth() > 7)
                        d.setMonth(d.getMonth() + 1);
                    StartDate = d.getFullYear() + '-' + (d.getMonth() + 1 + "").padStart('2', '0') + '-' + (d.getDate() + "").padStart('2', '0');


                    // the last payment is to march + number of payments - 1;
                    d.setMonth(d.getMonth() + payments - 1);
                    // one day past the last payment
                    d.setDate(d.getDate() + 1);
                    EndDate = d.getFullYear() + '-' + (d.getMonth() + 1 + "").padStart('2', '0') + '-' + (d.getDate() + "").padStart('2', '0');
                    paymentLeft = payments;
                }
                else {
                    // 7 is august. 1 payment for august, the other paymetns for the rest.

                    let maxPayments = 8 - d.getMonth();
                    if (maxPayments < payments)
                        throw "maximum payments now are " + maxPayments;

                    firstPayment = otherPayments = amount / payments;
                    // set the second payment after FirstPaymentDate;
                    d.setMonth(d.getMonth() + 1);
                    StartDate = d.getFullYear() + '-' + (d.getMonth() + 1 + "").padStart('2', '0') + '-' + (d.getDate() + "").padStart('2', '0');

                    // set the last payment (2 payments already passed!)
                    d.setMonth(d.getMonth() + payments - 2);
                    // one day past the last payment
                    d.setDate(d.getDate() + 1);
                    EndDate = d.getFullYear() + '-' + (d.getMonth() + 1 + "").padStart('2', '0') + '-' + (d.getDate() + "").padStart('2', '0');

                    paymentLeft = payments - 1;
                }


                // first payment immediately for the receipt.
                let paymentData = await callAPI("payment", {
                    AccountId: ccAccount,
                    Amount: firstPayment,
                    CVV: req.body.CVV,
                    PurchaseOrderNumber: saleRows[0].get("SaleId").id,
                    SuccessReceiptOptions: {
                        "SendToCustomer": true,
                    }
                }, "post");

                console.log(account.id, paymentData);

                let log = new (Simbla.Object.extend("PaymentsLog"));
                await log.save({
                    Method: "paysimple",
                    Total: firstPayment + "",
                    Currency: "USD",
                    Type: "first payment of recurring payment",
                    //TransactionType: transactionType,
                    User: req.body.user ? Simbla.User.createWithoutData(req.body.user.objectId) : undefined,
                    AccountId: account,
                    PaymentsNumber: Number(1),
                    Status: paymentData.ProviderAuthCode,
                    Data: paymentData,
                }, {
                    useMasterKey: true
                });

                /***
            * 
            * !!!! remove on prod!!!!!
            * 
            */
                if (ENV_KEY === "sandbox-") {
                    let x = new Date();
                    x.setDate(x.getDate() + 1);
                    StartDate = x.getFullYear() + '-' + (x.getMonth() + 1 + "").padStart('2', '0') + '-' + (x.getDate() + "").padStart('2', '0');
                    ExecutionFrequencyParameter = x.getDate();
                }


                // no payment is immediately. the second payment already schedule at StartDate
                paymentData = await callAPI("recurringpayment", {
                    AccountId: ccAccount,
                    //FirstPaymentAmount: 0,
                    //FirstPaymentDate,
                    ExecutionFrequencyType: "SpecificDayofMonth",
                    ExecutionFrequencyParameter,

                    PaymentAmount: otherPayments,
                    StartDate,
                    EndDate,
                    Description: saleRows[0].get("SaleId").id,

                }, "post");

                console.log(account.id, paymentData);
                await log.save({
                    Method: "paysimple",
                    Total: otherPayments + "",
                    Currency: "USD",
                    Type: "recurring payments",
                    //TransactionType: transactionType,
                    User: req.body.user ? Simbla.User.createWithoutData(req.body.user.objectId) : undefined,
                    AccountId: account,
                    PaymentsNumber: paymentLeft,
                    Status: paymentData.ScheduleStatus,
                    Data: paymentData,
                }, {
                    useMasterKey: true
                })
                // is this is the validation of success payment? 
                //                 if (paymentData.ProviderAuthCode === "Approved") {

                //                 }
            }
            else {

                const paymentData = await callAPI("payment", {
                    AccountId: ccAccount,
                    Amount: amount,
                    CVV: req.body.CVV,
                    PurchaseOrderNumber: saleRows[0].get("SaleId").id,
                    SuccessReceiptOptions: {
                        "SendToCustomer": true,
                    }
                }, "post");

                console.log(account.id, paymentData);
                let log = new (Simbla.Object.extend("PaymentsLog"));
                await log.save({
                    Method: "paysimple",
                    Total: amount + "",
                    Currency: "USD",
                    Type: "regular payment",
                    //TransactionType: transactionType,
                    User: req.body.user ? Simbla.User.createWithoutData(req.body.user.objectId) : undefined,
                    AccountId: account,
                    PaymentsNumber: Number(1),
                    Status: paymentData.ProviderAuthCode,
                    Data: paymentData,
                }, {
                    useMasterKey: true
                })
                // is this is the validation of success payment? 
                // if (paymentData.ProviderAuthCode === "Approved") {

                // }

            }

        }
        // IL/ EU purchas
        else {

            //let isEURPurchase;
            let DocumentDetails;
            //const currCode = isEURPurchase ? EURCODE : ILSCUDE;

            //if (saleRows.some(s => s.get("ProductId") && s.get("ProductId").get("CurrencyId") && s.get("ProductId").get("CurrencyId").id === CURRENCY_EUR))

            //const currRate = await getCurrentcyRate()

            let isDiasporaCard;
            let currentAmount = amount;
            if (payments && payments > 1) {

                isDiasporaCard = await getCardType(req.body.CreditCardNumber, req.body.ExpirationDate.substr(0, 2) + (req.body.ExpirationDate.substr(-2, 2)), req.body.CVV, undefined, savedToken && savedToken.get("Token"));
                if (payments > (isDiasporaCard ? 5 : 10))
                    throw "maximum payments are " + (isDiasporaCard ? 5 : 10);

                // todo set the payments
                //currentAmount = 
            }


            let chargeAmount = currentAmount;


            let pelecardPayments, transactionType;
            let createToken, paymentsFactor = 1;
            if (payments > 1) {

                if (isDiasporaCard) {
                    createToken = true;
                    chargeAmount = chargeAmount / payments;
                    paymentsFactor = payments;
                    DocumentDetails = `תשלום ראשון מתוך ${payments}`

                }
                else {
                    pelecardPayments = payments;
                    transactionType = "DebitPaymentsType";
                    DocumentDetails = `${payments} תשלומים`
                }
            }

            if (req.body.saveToken) {
                createToken = true;
            }
            let pelecardChargeAmount = chargeAmount * 100;

            /*******************************
             * 
             * !!!! remove on prod!!!!!
             * 
             */
            if (ENV_KEY === "sandbox-") {
                pelecardChargeAmount /= 1000;
            }

            /****
             * !!!!! remove on prod !!!
             */



            let chargeBody = {
                total: pelecardChargeAmount,
                currency: "1",
                paymentsNumber: pelecardPayments,
                transactionType,
                //creditCard: req.body.CreditCardNumber,
                //creditCardDateMmYy: req.body.ExpirationDate.substr(0, 2) + (req.body.ExpirationDate.substr(-2, 2)),
                //cvv2: req.body.CVV,

                //creditHolderName: paymentData.cardname,
                accountId: account.id,
                createToken: !!createToken
            };

            if (req.body.ccToken) {
                chargeBody.token = savedToken.get("Token");
                if (!chargeBody.token)
                    throw "invalid token";
            }
            else {
                chargeBody.creditCard = req.body.CreditCardNumber;
                chargeBody.creditCardDateMmYy = req.body.ExpirationDate.substr(0, 2) + (req.body.ExpirationDate.substr(-2, 2));
                chargeBody.cvv2 = req.body.CVV
            }
            let charge = await Simbla.Cloud.run("charge_creditcard", chargeBody);

            
                
            console.log(charge);
            if (charge.status !== "000")
                throw "charge fail";

            let cardType = checkPeleCardType(charge.CreditCardCompanyIssuer)

            let last4digit = req.body.CreditCardNumber.substr(-4)

            const settings = await (new Simbla.Query("AccountingSettings")).first({
                useMasterKey: true
            })

            let hasVAT = settings.get("VAT") && saleRows.some(s => s.get("ProductId").get("HasVat"))
                && saleRows.some(s => s.get("ContactId") && s.get("ContactId").get("LivesInIsrael") && s.get("ContactId").get("LivesInIsrael").id === "H2DCNDdHUE")



            let VatValue = 0, VatPercent = 0;
            let priceFactor = 1;
            if (hasVAT) {
                VatPercent = settings.get("VAT");

                priceFactor = 100 / (VatPercent + 100);  //1 - (VatPercent / 100)

                VatValue = currentAmount - (currentAmount * priceFactor);

                console.log({ VatPercent, VatValue, priceFactor, currentAmount })
            }

            if (payments > 1 && isDiasporaCard) {

                if (VatValue)
                    VatValue = VatValue / payments;

                //  create retainer
                let token = req.body.ccToken || (charge && charge.accountToken && charge.accountToken.id);
                if (!token) {

                    throw "Cannot create retainer without token";

                }

                let tokenToSave
                if (token) {
                    tokenToSave = await Simbla.Object.extend("AccountTokens").createWithoutData(token).fetch({ useMasterKey: true });
                    tokenToSave.set("CreditCardType", Simbla.Object.extend("CreditCardTypes").createWithoutData(cardType))
                    await tokenToSave.save(null, { useMasterKey: true });
                }

                // create reitainer with hkTerminal


                let NextChargeDate = new Date();
                NextChargeDate.setMonth(NextChargeDate.getMonth() + 1);

                let retainerData = {
                    AccountId: account,
                    NextChargeDate,
                    Active: true,
                    DocTypeId: Simbla.Object.extend("AccountingDocsType").createWithoutData(TaxInvoiceReceiptId),
                    DiscountValue: 0,
                    TotalBeforeDiscount: chargeAmount - VatValue,
                    TotalAfterDiscount: chargeAmount - VatValue,
                    VatValue: VatValue,
                    VatPercent: VatPercent,
                    NoVat: VatPercent === 0,
                    TotalSum: chargeAmount,
                    SaleId: sale,

                    RetainerPeriod: 1,
                    ChargesLimit: payments - 1,



                    AccountTokenId: {
                        objectId: token,
                        __type: "Pointer",
                        className: "AccountTokens"
                    },
                    PaymentRowType: {
                        objectId: "Rl0OajXxax",
                        __type: "Pointer",
                        className: "PaymentType"
                    }, //credit-card id             
                    PaymentDescription: "אשראי",
                    Email: account.get("Email").trim() || undefined,

                }
                if (req.body.EN_template)
                    retainerData.TemplateCode = InvoiceReceiptTemplateIdEN;

                let retainer = new (Simbla.Object.extend("Retainers"));
                await retainer.save(retainerData, { useMasterKey: true });

                for (let i = 0; i < saleRows.length; i++) {
                    let row = saleRows[i];
                    if (row.get("Total") == 0)
                        continue;

                    const retainerRow = new (Simbla.Object.extend("RetainerRows"));

                    retainerRow.set("Quantity", Number(row.get("Quantity")));
                    retainerRow.set("Price", row.get("PricePerUnit") * (priceFactor || 1) / paymentsFactor)
                    retainerRow.set("RowTotal", (row.get("RowTotal") || row.get("Total")) * (priceFactor || 1) / paymentsFactor);
                    retainerRow.set("ProductId", row.get("ProductId"));
                    retainerRow.set("RetainerId", retainer);
                    retainerRow.set("CurrencyRate", row.get("CurrencyRate") || 1);
                    retainerRow.set("CurrencyId", row.get("CurrencyId"))

                    await retainerRow.save(null, { useMasterKey: true });
                }
                console.log('retainer created successfully', retainer.toJSON());


            }
            //  create heb document

            let documentData = dataForCreateHebDocument({
                last4digit,
                cardType,
                accountId: account.id,
                totalSum: chargeAmount,
                DiscountValue: 0,
                TotalBeforeDiscount: (chargeAmount - VatValue),
                TotalAfterDiscount: (chargeAmount - VatValue),
                VatValue: Number(VatValue.toFixed(2)),
                VatPercent: VatPercent,
                NoVat: !hasVAT,
                name: req.body.EN_template ? "To " + account.get("familyName") + " family" : "לכבוד משפחת " + account.get("familyName"),
                address: account.get("Address"),
                zip: account.get("Zip"),
                country: account.get("Country") || (account.get("CountryId") && account.get("CountryId").get("Name")) || "",
                state: "",
                city: account.get("City"),
                phone: account.get("PhoneNumber"),
                email: account.get("Email"),
                DebitApproveNumber: charge && charge.Results && charge.Results.DebitApproveNumber,
                priceFactor: priceFactor / paymentsFactor,
                DocumentDetails,
                EN_temp: req.body.EN_template,
            }, undefined, saleRows);

            //console.log('data for create document', data);
            let createDoc = await Simbla.Cloud.run("createHebDocument", documentData, {
                useMasterKey: true
            });
            console.log("Document created successfully");

            if (account.get("Email")) {
                console.log('seding doc to', account.get("Email"));
                Simbla.Cloud.run("sendHebDocument", {
                    emails: [account.get("Email")],
                    company: settings.get("NameHeb"),
                    id: createDoc.documentId
                }, {
                    useMasterKey: true
                }).catch(e => console.error(e));
            }
            let header = Simbla.Object.extend("AccountingHeaders").createWithoutData(createDoc.documentId)
            header.set("SaleId", saleRows[0].get("SaleId"))
            header.save(null, { useMasterKey: true })

            saleCurrncy = CURRENCIES.createWithoutData(CURRENCY_IL)


        }

        //  mark sale and sale rows as done sales
        // SaleRowStatus
        // cls: SaleRowStatusList
        // id: 

        const filterSaleRowsReturningContact = function (sr) {
            return sr.get("ProductId") &&
                sr.get("ProductId").id === ReturningContactDiscountProductId &&
                sr.get("ContactId") &&
                (!sr.get("ContactId").get("CamperStatus") || sr.get("ContactId").get("CamperStatus").id !== "jZ12O0CF2O")
        }


        let saleSave = saleRows[0].get("SaleId")
        let saveObj = {
            ApprovedTerms: req.body.ApprovedTerms,
            SaleStatusId: Simbla.Object.extend("SaleStatuses").createWithoutData("zrP1MSVBoq"),
            CurrencyId: saleCurrncy,
            Total: amount,
            ClosingDate: new Date()
        }
        if (req.body.SubSaleStatus) { /// נרשם נציג
            saveObj.SubRegisteredStatus = Simbla.Object.extend("SubRegisteredStatusList").createWithoutData(req.body.SubSaleStatus)
        }
        else { /// נרשם אתר
            saveObj.SubRegisteredStatus = Simbla.Object.extend("SubRegisteredStatusList").createWithoutData("oG3IZLGFfy")
        }

        await Promise.all([
            saleSave.save(saveObj, { useMasterKey: true }),
            // saleRows[0].get("SaleId").save({
            //     ApprovedTerms: req.body.ApprovedTerms,
            //     SubRegisteredStatus: Simbla.Object.extend("SubRegisteredStatusList").createWithoutData("oG3IZLGFfy"),
            //     SaleStatusId: Simbla.Object.extend("SaleStatuses").createWithoutData("zrP1MSVBoq"),
            //     CurrencyId: saleCurrncy,
            //     Total: amount,
            //     ClosingDate: new Date()
            // }, { useMasterKey: true }),
            Promise.all(saleRows.map(sr => sr.save({ SaleRowStatus: Simbla.Object.extend("SaleRowStatusList").createWithoutData("9NP9DJ5t1I") }, { useMasterKey: true }))),
            //  update coupon usage
            coupon ? coupon.increment("UsageAvailable", -1).save(null, { useMasterKey: true }) : Promise.resolve(),
            Promise.all(saleRows.filter(filterSaleRowsReturningContact).map(sr => {
                return sr.get("ContactId").save({ "CamperStatus": Simbla.Object.extend("CamperStatusList").createWithoutData("jZ12O0CF2O") }, { useMasterKey: true });
            }))

        ]);
        try {
            let sendEmail = await Simbla.Cloud.run("run_function", { functionName: "Kimama-sendEmail", saleId: saleSave.id }, {
                useMasterKey: true
            });
        }
        catch (err) {
            console.error(err)
        }

        return res.status(200).send('success');

    }
    catch (err) {
        if (err.isAxiosError) {
            console.error(err.response.statusText, err.response.data && err.response.data.Meta && err.response.data.Meta.Errors);
            return res.status(500).send((err.response.data && err.response.data.Meta && err.response.data.Meta.Errors) || err.response.statusText);

        }
        console.error(err);
        let message = (err.message && err.message.message) || err.message || err;
        res.status(500).send(message);
    }
};